import React, { useState } from "react";

import { StyleSheet, View } from "react-native";

import GameSettingsScreen from "./screens/setGameScreen";
import GamePlay from "./screens/GamePlay";

export default function App() {
  const [gameScreen, setGameScreen] = useState("Settings");
  const [roundTime, setRoundTime] = useState("");
  const [gameDifficulty, setGameDifficulty] = useState("");

  const gamePlayHandler = (roundTime) => {
    setRoundTime(roundTime);
    setGameScreen("Play");
  };

  const difficultyChoiceHandler = (difficulty) => {
    alert(difficulty)
    setGameDifficulty(difficulty);
    setSettingsPhase("Round Time");
  };
  let content =
    gameScreen === "Settings" ? (
      <GameSettingsScreen
        setDifficulty={difficultyChoiceHandler}
        settingsDone={gamePlayHandler}
      />
    ) : (
      <GamePlay roundTime={roundTime} difficulty={gameDifficulty} />
    );

  return <View style={styles.container}>{content}</View>;
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flex: 1,
  },
});
