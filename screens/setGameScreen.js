import React from "react";
import { View, StyleSheet, Text } from "react-native";

import Header from "../components/Header";
import GameSettings from "../components/GameSettings";

const SetGameScreen = (props) => {
  return (
    <View style={styles.setGameContainer}>
      <Header style={styles.header}>
        <Text style={styles.headerText}>Questions</Text>
      </Header>
      <GameSettings
        title={"Game Settings"}
        style={styles.settings}
        selectDifficulty={props.setDifficulty}
        onSettingsDone={props.settingsDone}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  setGameContainer: {
    justifyContent: "center",
    width: "100%",
    height: "100%",
  },
  header: {
    flex: 1,
  },
  headerText: {
    color: "white",
    fontSize: 28,
  },
  settings: {
    flex: 2,
    // backgroundColor:'pink'
  },
});
export default SetGameScreen;
