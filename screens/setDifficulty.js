import React from "react";

import { StyleSheet, TouchableOpacity, Text } from "react-native";
import Card from '../components/Card'
import Colors from '../constants/Colors'
import TouchableStyle from '../constants/Touchable'
const Difficulty = (props) => {
  return (
    <Card style={props.cardStyle}>
      <TouchableOpacity style={TouchableStyle.button} onPress={() => props.difficultyChoice('easy')}>
        <Text style={TouchableStyle.text}>Piece of Cake</Text>
      </TouchableOpacity>

      <TouchableOpacity style={TouchableStyle.button} onPress={() => props.difficultyChoice('medium')}>
        <Text style={TouchableStyle.text}>Testing</Text>
      </TouchableOpacity>

      <TouchableOpacity style={TouchableStyle.button} onPress={() => props.difficultyChoice('hard')}>
        <Text style={TouchableStyle.text}>No Go !</Text>
      </TouchableOpacity>
    </Card>
  );
};

const styles = StyleSheet.create({
    card:{ },
});
export default Difficulty;
