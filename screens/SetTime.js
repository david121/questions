import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import Card from "../components/Card";
import TouchableStyle from "../constants/Touchable";
const SetGameTime = (props) => {
  return (
    <Card style={props.cardStyle}>
      <TouchableOpacity style={TouchableStyle.button}
        onPress={()=>props.gameReady('15')}>
        <Text style={TouchableStyle.text}>15 Seconds</Text>
      </TouchableOpacity>
      <TouchableOpacity style={TouchableStyle.button}
        onPress={()=>props.gameReady('30')}>
        <Text style={TouchableStyle.text}>30 Seconds</Text>
      </TouchableOpacity>
      <TouchableOpacity style={TouchableStyle.button}
        onPress={()=>props.gameReady('60')}>
        <Text style={TouchableStyle.text}>1 Minute</Text>
      </TouchableOpacity>
    </Card>
  );
};

const styles = StyleSheet.create({});

export default SetGameTime;
