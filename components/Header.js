import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Colors from '../constants/Colors'
const Header = props => {
  return (
    <View style={{...styles.header, ...props.style}}>
      {props.children}
    </View>
  );
};

const styles = StyleSheet.create({
    header:{
         width:'100%',
         backgroundColor:Colors.primary,
         justifyContent:"center",
         alignItems:'center',
    } 
});

export default Header;
