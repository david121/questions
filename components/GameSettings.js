import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";

import Difficulty from "../screens/setDifficulty";
import SetGameTime from "../screens/SetTime";
import Colors from "../constants/Colors";

const GameSettings = (props) => {
  const [settingsPhase, setSettingsPhase] = useState("Difficulty");

  let settingsPhaseComponent =
    settingsPhase === "Difficulty" ? (
      <Difficulty
        cardStyle={styles.card}
        difficultyChoice={{...props.selectDifficulty,...setSettingsPhase('Time')}}
      />
    ) : (
      <SetGameTime cardStyle={styles.card} gameReady={props.onSettingsDone}/>
    );

  return (
    <View style={{ ...styles.settingsContainer, ...props.style }}>
      <View style={styles.settingsHeader}>
        {/* <Text style={styles.settingsHeaderText}>{props.title}</Text> */}
        <View style={styles.settingsStage}>
          <Text style={styles.settingsPhaseText}>{settingsPhase}</Text>
        </View>
      </View>
      {settingsPhaseComponent}

      <View>
        {settingsPhase !== "Difficulty" ? (
          <View>
            <TouchableOpacity style={styles.backButton} onPress={() => setSettingsPhase('Difficulty')}>
              <Text style={styles.backButtonText}> { ' <-- ' }
                {/* {gameDifficulty.toUpperCase()} */}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  settingsContainer: {
    width: "100%",
    alignItems: "center",
  },
  settingsHeader: {
    padding: 10,
    marginVertical: 5,
    alignItems: "center",
  },
  settingsHeaderText: {
    fontSize: 20,
  },
  settingsStage: {
    marginVertical: 20,
  },
  settingsPhaseText: {
    textDecorationLine: "underline",
  },
  card: {
    marginTop: 5,
    width: 300,
    padding: 10,
    maxWidth: "80%",
    alignItems: "center",
  },
  backButton: {
    minWidth: 100,
    backgroundColor: Colors.primary,
    padding: 10,
    marginTop: 20,
    alignItems: "center",
  },
  backButtonText: {
    color: "white",
    fontSize: 15,
  },
});

export default GameSettings;
