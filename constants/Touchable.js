import Colors from './Colors'
const Touchable = {
    button : {
        width: "40%",
        justifyContent: "center",
        alignItems: "center",
        padding: 10,
        backgroundColor: Colors.secondary,
        height: 35,
        marginVertical: 10,
        borderEndWidth: 2,
        borderEndColor: Colors.primary,
        borderBottomEndRadius: 15,
    },
    text:{
        color: "#4d3546",
    }
}

export default Touchable;